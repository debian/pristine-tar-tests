# pristine-tar problematic tarballs

This repository stores tarballs that pristine-tar currently fails to reproduce,
or failed to in the past, to be used as a regression test suite.

# Running tarball tests

To run tests against all tarballs stored in this repository:

    ./test-tarballs

To test specific tarballs:

    ./test-tarballs /path/to/tarball [...]

# Running repository operation tests

To run tests against all repositories stored in this repository:

    ./test-repos

To test specific repos:

    ./test-repos /path/to/repo.bundle [...]

# Generating the HTML page with historical data

Just run `make`. The resulting HTML file will be `history/index.html`.

There are two ways of adding entries for new versions of pristine-tar:

* If you have a version of pristine-tar that does not have historical data yet
  (see `history/*.yaml`), just run:

  ```
  make run
  ```

* If you want to test a locally-modified version of pristine-tar (e.g. you are
  hacking on it), run:

  ```
  /path/to/pristine-tar-checkout/run make run
  ```

  Or, to test a single tarball:

  ```
  /path/to/pristine-tar-checkout/run ./test-tarballs TARBALL
  ```

  Or, to test a single repository:

  ```
  /path/to/pristine-tar-checkout/run ./test-tarballs REPO.bundle
  ```

The historical data is currently published at
https://people.debian.org/~terceiro/pristine-tar/.

# About

This repository started as a separate branch in Joey Hess's original
pristine-tar repository. This has beem moved into a different repository to
avoid pulling all of the data here in all pristine-tar clones.
