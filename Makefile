all: history/index.html

-include .git/config.mk

history := $(shell ls -1 history/*.yaml | sort -V)
history/index.html: $(history)
	@dpkg-query --show libyaml-perl
	./gen-html $(history) > $@ || ($(RM) $@; false)

history/index.html: gen-html

PRISTINE_TAR_VERSION ?= $(shell dpkg-query --show pristine-tar | awk '{print($$2)}')

run: history/$(PRISTINE_TAR_VERSION).yaml
	$(MAKE)

history/$(PRISTINE_TAR_VERSION).yaml:
	./testall | tee $@
	$(MAKE)

clean:
	$(RM) history/index.html
